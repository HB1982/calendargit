<?php
require_once __DIR__ . '/vendor/autoload.php';
error_reporting(E_ALL);
ini_set("display_errors", 1);

$client = new Google_Client();

$client->setAuthConfigFile('credentials.json');
$client->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '/calendar/quickstart/oauth2callback.php');
$client->addScope(Google_Service_Calendar::CALENDAR);
$client->addScope(Google_Service_Oauth2::USERINFO_PROFILE);
$client->addScope(Google_Service_Oauth2::USERINFO_EMAIL);
$_SESSION['access_token'] = $client->getAccessToken();
$oauthService = new Google_Service_Oauth2($client);
if (!isset($_GET['code'])) {
    $auth_url = $client->createAuthUrl();

    header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
} else {

    session_start();
    $client->authenticate($_GET['code']);
    $_SESSION['access_token'] = $client->getAccessToken();
    $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/calendar/quickstart/quickstart.php';
    $userInfo = $oauthService->userinfo_v2_me->get();
    echo "User info:<br>Name: " . $userInfo->email;
    $_SESSION['name'] = $userInfo->email;
    header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));

}