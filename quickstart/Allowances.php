<?php

class Allowances
{
    private $db;

    public function __construct()
    {
        $this->db = $this->connect();
    }

    public function connect()
    {

        try {
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $db = new PDO('mysql:host=localhost;dbname=sc4fkfd9042_calendar', 'helene', 'BEURY', $pdo_options);        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
        return $db;
    }

    public function getDistanceAPIAR($destination)
    {
        $address = urlencode("12 Cours de la République, 11200 Lézignan-Corbières");
        $destination = urlencode($destination);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://maps.googleapis.com/maps/api/distancematrix/json?destinations=$destination&origins=$address&units=imperial&key=AIzaSyD7JExPMrArci-1YUMCbTGqFcevBmvPpSk");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        $result = json_decode($response);
        curl_close($ch);
        if ($result->rows[0]->elements[0]->status!='OK'){
            return null;
        }else {


            $distance = round($result->rows[0]->elements[0]->distance->value / 1000 * 2, 2);
        }

        return $distance;
    }

    public function addDriver($name, $powerfull)
    {
        $req = $this->db->prepare('INSERT INTO driver (name,powerfull) VALUES (?,?)');
        $req->execute([$name, $powerfull]);
    }

    public function insertEvent($departure, $arrival, $distance, $date, $city, $idEvent, $event, $user)
    {
        $firstreq = $this->db->prepare('select * from trajets where ref=?');
        $firstreq->execute([$idEvent]);
        $res = $firstreq->fetch();
        if (empty($res)) {
            $req = $this->db->prepare('INSERT INTO trajets (departure,arrival,distance,date,city,ref,event,user) VALUES (?,?,?,?,?,?,?,?)');
            $req->execute([$departure, $arrival, $distance, $date, $city, $idEvent, $event, $user]);
        }
    }

    public function getDistance($destination)
    {
        $address = urlencode("12 Cours de la République, 11200 Lézignan-Corbières");
        $distance = false;
        $req = $this->db->prepare('select distance from destinations where destination=?');
        $req->execute([$destination]);
        $result = $req->fetch();
        if ($result!=false) {
            $distance = $result['distance'];

        }
        if ($distance===false) {
            $distance = $this->getDistanceAPIAR($destination);
            $req = $this->db->prepare('INSERT INTO destinations (destination,distance,departure) VALUES (?,?,?) ');
            $req->execute([$destination, $distance, $address]);
        }
        return $distance;
    }


    public function getRouteByUser($user, $start, $end)
    {
        $req = $this->db->prepare("SELECT * FROM trajets WHERE user=?  and date between ? and ?");
        $req->execute([$user, $start, $end]);
        $res = $req->fetchAll();
        return $res;

    }

    public function getAllUsers()
    {
        $req = $this->db->prepare("SELECT DISTINCT user FROM trajets ");
        $req->execute([]);
        $res = $req->fetchAll();
        return $res;
    }


    public function deleteEvent($id)
    {
        $req = $this->db->prepare("DELETE FROM trajets where ref=? ");
        $req->execute([$id]);

    }

    public function isPro($id)
    {
        $req = $this->db->prepare("SELECT * from trajets where ref=? ");
        $req->execute([$id]);
        $res = $req->fetch();
        return $res;
    }

}

