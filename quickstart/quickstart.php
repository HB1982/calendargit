<!doctype html>
<html lang="en">
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx"
          crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,200;1,200&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="style.css" rel="stylesheet">
    <title>Indemnités kilométriques</title>
</head>
<body>
<?php
include 'Allowances.php';
include 'Ik.php';
require_once __DIR__ . '/vendor/autoload.php';
error_reporting(E_ALL);
ini_set("display_errors", 1);

use Google\Client;
use Google\Service\Calendar;

$client = new Google_Client();
session_start();
$client->addScope(Google_Service_Oauth2::USERINFO_PROFILE);
$client->addScope(Google_Service_Oauth2::USERINFO_EMAIL);
?>

<nav class="navbar navbar-expand-md  navbar-dark">
    <a class="navbar-brand" href="#">IK</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Accueil</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="quickstart.php">Voir mes déplacements professionnels</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="myEvents.php">Calculez mes indemnités kilométriques</a>
            </li>

        </ul>
    </div>
</nav>
<?php
if (($_SESSION['name']) == 'helenebeury82@gmail.com') {
    ?>
    <a class="admin" href="users.php">Accès administrateur</a>
    <a class="admin" href="send.php">Inviter un utilisateur</a>
    <?php
}
?>
<section class="container">
    <h1 style="text-align: center">MERCI DE SELECTIONNER VOS RENDEZ-VOUS PROFESSIONNELS</h1>
    <a href="?logout">LOG OUT</a>
    <?php
    $function = new Ik();
    $function->ifLogout();
    if (isset($_SESSION['access_token']) && !empty($_SESSION['access_token'])) {
        $client = $function->getClient($_SESSION['access_token']);

        $service = new Calendar($client);

        if (!isset($_GET['date'])) $_GET['date'] =
        $_GET['date'] = date('Y-m');

        $timemin = strtotime(date('Y-m-d', strtotime('first day of ' . $_GET['date'])));
        $timemax = strtotime(date('Y-m-d', strtotime('last day of ' . $_GET['date'])));
        ?>
        <a href="quickstart.php?date=<?= date('Y-m-d', strtotime('-1 month', strtotime($_GET['date']))) ?>"
           title="C'est GET">Mois précédent</a>
        <a href="quickstart.php?date=<?= date('Y-m-d', strtotime('+1 month', strtotime($_GET['date']))) ?>"
           title="C'est GET">Mois suivant</a>
        <h2>Période du <?= date('d-m-y', strtotime('first day of ' . $_GET['date'])) ?>
            au <?= date('d-m-y', strtotime('last day of ' . $_GET['date'])) ?> </h2>
    <?php
    try {

    $function->ckeckPost();
    ?>
    <?php
    $calendarId = 'primary';
    $optParams = array(
        'maxResults' => 200,
        'orderBy' => 'startTime',
        'singleEvents' => true,
        'timeMin' => date('c', $timemin),
        'timeMax' => date("c", $timemax),
    );
    $results = $service->events->listEvents($calendarId, $optParams);
    $events = $results->getItems();
    $adress = urlencode("12 Cours de la République, 11200 Lézignan-Corbières");
    if (empty($events)) {
        print "No upcoming events found.\n";
    } else {
    $myAllowance = new Allowances();
    ?>

        <form id="form" method="post" action="">
            <table class="calendar">
                <thead>
                <tr>
                    <th colspan="5">Trajets du mois</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>User</td>
                    <td>Date</td>
                    <td>Evenement</td>
                    <td>Distance A/R</td>
                    <td>Sélectionner vos déplacements professionnels</td>
                </tr>


                <?php

                $address = urlencode("12 Cours de la République, 11200 Lézignan-Corbières");
                foreach ($events as $event) {
                    if (empty($event->getLocation())) {
                        continue;
                    }
                    if (empty($start)) {
                        $start = $event->start->date;
                    }
                    $location = urlencode($event->getLocation());
                    $id = $event->getId();
                    $event_distance = $myAllowance->getDistance($event->getLocation());
                    $isPro = $myAllowance->isPro($id);
                    if (isset($_POST[$id])) {
                        $value = (bool)$_POST[$id];
                        if ($value == true) {
                            $insertBdd = $myAllowance->insertEvent($adress, $event->getLocation(), $event_distance, $event->getStart()->getDateTime(), $event->getLocation(), $id, $event->getSummary(), $_SESSION['name']);
                        } else {
                            $routes = $myAllowance->deleteEvent($id);
                        }
                        header('Location: myEvents.php?date=' . $_GET['date']);
                    }
                    ?>
                    <tr>
                        <td><?= $_SESSION['name'] ?></td>
                        <td> <?= date('d-m-Y', strtotime($event->getStart()->getDateTime())) ?></td>
                        <td><?= $event->getSummary() . ' : ' . $event->getLocation() ?></td>
                        <?php if ($event_distance == 0) { ?>
                            <td> non disponible</td>
                        <?php } else { ?>
                            <td><?= $event_distance ?> km</td> <?php } ?>
                        <?php if ($event_distance == 0) { ?>
                            <td style="background:darkslategrey;"></td>
                        <?php } else { ?>
                            <td>RDV professionnel : <br><input type="radio" name='<?= $id ?>'
                                                               value='1' <?php if ($isPro) echo "checked='checked'"; ?>>Oui<br>
                                <br><input id=<?= $id ?> type="radio" name='<?= $id ?>'
                                           value='0' <?php if (!$isPro) echo "checked=''"; ?>>Non<br>
                            </td> <?php } ?>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
            <input id='button' class=calcul type=submit value='Submit' name="submit">
        </form>
        <script>
            let myForm = document.querySelector('form');
            myForm.addEventListener("submit", function () {
                alert("Vos déplacements ont bien été enregistrés");
            });
        </script>
    <br>
        <?php
    }
    } catch
    (Exception $e) {
        // TODO(developer) - handle error appropriately
        echo 'Message: ' . $e->getMessage();
    }
    } else {
        session_destroy();
        unset($_COOKIE);
        $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/calendar/quickstart/oauth2callback.php';
        header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
    } ?>
</section>
</body>
</html>