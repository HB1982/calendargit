<?php

require __DIR__ . '/vendor/autoload.php';


use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;


try {
    ob_start();
    $style = "<style>


    .calendar {
        margin-top: 20px;
    }

    .calendar td, .calendar th {
        border: 1px solid #ddd;
        padding: 8px;
    }

    .calendar tr:nth-child(even) {
        background-color: #f2f2f2;
    }

    .calendar tr:hover {
        background-color: #ddd;
    }

    .calendar th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #04AA6D;
        color: white;
    }
    .distance {
     
        display: inline-block;
        padding-block: 20px;
        padding-inline: 10px;
        color: black;
        border-radius: 10px;
        font-size:20px;
        
    }

    .ik {
    background:#095e55;
   
        display: inline-block;
        padding-top: 20px;
        padding-right: 10px;
        margin-left: 10px;
        color: white;;
        border-radius: 10px;
        font-size: 30px;
        text-align: center;
        font-weight: 800;
    }

a{
    display:none;
    color:white;

}

ul li {
   color:white;}



ul.nav{
 opacity: 0!important;
 height: 0!important;
 overflow: hidden!important;
}

</style>
";
    echo $style; ?>
    <!doctype html>
    <html lang="en">

    <head>

    <body class="container">
    <?php
    include 'Allowances.php';
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    session_start();

    if (!isset($_GET['date'])) {
        $_GET['date'] = date('Y-m');
    }

    else{
        echo date('Y-m-d', strtotime('first day of '.$_GET['date']));
        echo date('Y-m-d', strtotime('last day of '.$_GET['date']));
    }
    ?>

    <h2>Période du <?= date('Y-m-d', strtotime('first day of '.$_GET['date'])) ?> au <?= date('Y-m-d', strtotime('last day of '.$_GET['date'])) ?></h2>
    <a href="test.php?date=<?=date('Y-m-d',strtotime('-1 month',strtotime($_GET['date'])))?>"
       title="C'est GET">Mois précédent</a>
    <a href="test.php?date=<?= date('Y-m-d',strtotime('+1 month',strtotime($_GET['date']))) ?> "
       title="C'est GET">Mois suivant</a>

    <?php
    $start =date('Y-m-d', strtotime('first day of '.$_GET['date'])) ;
    $end = date('Y-m-d', strtotime('last day of '.$_GET['date']));
    $myAllowance = new Allowances();
    $useBdd = $myAllowance->getRouteByUser($_SESSION['name'], $start, $end);
    $total = 0;
    ?>

    <table class="calendar">
        <thead>
        <tr>
            <th colspan="4">trajets professionnels du mois</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Utilisateur</td>
            <td>Date</td>
            <td>Evènement</td>
            <td>Distance A/R</td>
        </tr>
        <?php
        foreach ($useBdd as $route) {

            ?>
            <tr>
                <td><?= $route['user'] ?></td>
                <td><?= $route['date'] ?></td>
                <td><?= $route['event'] . ':' . $route['city'] ?></td>
                <td><?= $route['distance'] ?> km</td>
            </tr>
            <?php
            $total += $route['distance'];
            $allowances = round($total * 0.575, 2);

        } ?>
        </tbody>
    </table>

    <p class="distance"> Distance parcourue: <?= (empty($total) ? "0" : $total) ?>km</p><br>

    <div class="ik">
        <p> Indemnités kilométriques: <?= (empty($allowances) ? "0" : $allowances) ?>€</p><br>
    </div>
    </body>
    </html>

    <?php
    $content = ob_get_clean();

    $html2pdf = new Html2Pdf('P', 'A4', 'fr');
    $html2pdf->setTestTdInOnePage(false);
    $html2pdf->setDefaultFont('Arial');
    $html2pdf->writeHTML($content);
    $html2pdf->output('example00.pdf');

} catch (Html2PdfException $e) {
   $html2pdf->clean();

    $formatter = new ExceptionFormatter($e);
    echo $formatter->getHtmlMessage();
}

