<!doctype html>
<html lang="en">

<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa"
            crossorigin="anonymous"></script>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link href="style.css" rel="stylesheet">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<section class="container"></section>
<?php
include 'Allowances.php';
error_reporting(E_ALL);
ini_set("display_errors", 1);
session_start();
if (!isset($_GET['date'])) {
    $_GET['date'] = strtotime('now');
}
?>


<a href="myEvents.php?date=<?= strtotime(date('Y-m-d', $_GET['date']) . ' -1 month') ?>"
   title="C'est GET">PREVIEW</a>
<a href="myEvents.php?date=<?= strtotime(date('Y-m-d', $_GET['date']) . ' +1 month') ?>"
   title="C'est GET">NEXT</a>

<?php
$timemin = strtotime(date('Y-m-d', $_GET['date']) . ' -1 month');
$start = date('c', $timemin);
$end = date("c", $_GET['date']);
$myAllowance = new Allowances();
$useBdd = $myAllowance->getRouteByUser($_SESSION['name'], date('', $timemin), date("c", $_GET['date']));
$total = 0;
?>

<table class="calendar">
    <thead>
    <tr>
        <th colspan="4">trajets du mois</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Utilisateur</td>
        <td>Date</td>
        <td>Evenement</td>
        <td>Distance A/R</td>
    </tr>
    <?php
    foreach ($useBdd as $route) {

        ?>
        <tr>
            <td><?= $route['user'] ?></td>
            <td><?= $route['date'] ?></td>
            <td><?= $route['event'] . ':' . $route['city'] ?></td>
            <td><?= $route['distance'] ?> km</td>
        </tr>
        <?php
        $total += $route['distance'];
        $allowances = round($total * 0.575, 2);

    } ?>
    </tbody>
</table>
<p class="distance"> Distance parcourue: <?= (empty($total) ? "0" : $total) ?>km</p><br>
<p class="ik"> Indemnités kilométriques: <?= (empty($allowances) ? "0" : $allowances) ?>€</p><br>

</body>
</html>

