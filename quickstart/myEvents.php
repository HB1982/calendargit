<!doctype html>
<html lang="en">

<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa"
            crossorigin="anonymous"></script>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
    <link href="style.css" rel="stylesheet">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
include 'Allowances.php';
error_reporting(E_ALL);
ini_set("display_errors", 1);
session_start();

 if (!isset($_GET['date'])) $_GET['date'] =
 $_GET['date'] = date('Y-m');
        ?>


<nav class="navbar navbar-expand-md  navbar-dark">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="collapsibleNavbar">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Accueil</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="quickstart.php">Voir mes déplacements professionnels</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="myEvents.php">Calculez mes indemnités kilométriques</a>
            </li>

        </ul>
    </div>
</nav>

<?php
if (($_SESSION['name']) == 'helenebeury82@gmail.com') {
    ?>
    <a class="admin" href="users.php">Accès administrateur</a>
    <a class="admin" href="send.php">Inviter un utilisateur</a>
    <?php
}
?>
<section class="container">
    <h2>Période du <?= date('d-m-y', strtotime('first day of ' . $_GET['date'])) ?>
        au <?= date('d-m-y', strtotime('last day of ' . $_GET['date'])) ?> </h2>
    <a href="myEvents.php?date=<?= date('Y-m-d',strtotime('-1 month',strtotime($_GET['date']))) ?>"
       title="C'est GET">Mois précédent</a>
    <a href="myEvents.php?date=<?=date('Y-m-d',strtotime('+1 month',strtotime($_GET['date']))) ?>"
       title="C'est GET">Mois suivant</a>

    <?php
    $timemin = strtotime(date('Y-m-d', strtotime('first day of '.$_GET['date'])));
    $timemax=strtotime(date('Y-m-d', strtotime('last day of '.$_GET['date']))) ;
    $myAllowance = new Allowances();
    $useBdd = $myAllowance->getRouteByUser($_SESSION['name'],  date('Y-m-d', $timemin), date("Y-m-d",$timemax));
    $total = 0;

    ?>

    <table class="calendar">
        <thead>
        <tr>
            <th colspan="4">Trajets professionels du mois</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Utilisateur</td>
            <td>Date</td>
            <td>Evenement</td>
            <td>Distance A/R</td>
        </tr>
        <?php
        foreach ($useBdd as $route) {

            ?>
            <tr>
                <td><?= $route['user'] ?></td>
                <td><?= $route['date'] ?></td>
                <td><?= $route['event'] . ':' . $route['city'] ?></td>
                <td><?= $route['distance'] ?> km</td>
            </tr>
            <?php
            $total += $route['distance'];
            $allowances = round($total * 0.575, 2);

        } ?>
        </tbody>
    </table>
    <p class="distance"> Distance parcourue: <?= (empty($total) ? "0" : $total) ?>km</p><br>
    <p class="ik"> Indemnités kilométriques: <?= (empty($allowances) ? "0" : $allowances) ?>€</p><br>
    <a target="_blank" href="testpdf.php/?date=<?= $_GET['date']?>">Télécharger PDF</a>
</section>
</body>
</html>

